﻿using System.Data.Entity;
using ArchitechTest.Data;
using ArchitechTest.Model;
using Autofac;

namespace ArchitechTest.Web.Controllers
{
    public class RepostioryFactory : IRepositoryFactory
    {
        private readonly ILifetimeScope container;

        public RepostioryFactory(ILifetimeScope container)
        {
            this.container = container;
        }

        public IRepository<TEntity> GetRepositoryForEntityType<TEntity>(DbContext dbContext) where TEntity : class
        {
            return container.Resolve<IRepository<TEntity>>(new NamedParameter("database", dbContext));
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ArchitechTest.Model;
using ArchitechTest.Web.ViewModels;

namespace ArchitechTest.Web.Controllers
{
    public class UserController : Controller
    {
        private readonly IUnitOfWork unitOfWork;

        public UserController(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        
        public ActionResult Index()
        {
            var viewModel = unitOfWork.Users.GetAll()
                .ToList()
                .Select(item => new UserViewModel(item.Id, item.Name));
            return View(viewModel);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CreateUserViewModel user)
        {
            if (ModelState.IsValid)
            {
                unitOfWork.Users.Add(user.ToModel());
                unitOfWork.Commit();
            }

            user.Success = true;
            return View(user);
        }

        
        public ActionResult CheckUserUniqueness(string name)
        {
            var exists = unitOfWork.Users.GetAll().Any(item =>
                item.Name.Equals(name, StringComparison.OrdinalIgnoreCase)
                );

            return Json(!exists, JsonRequestBehavior.AllowGet);
        }
    }
}
﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using ArchitechTest.Model;

namespace ArchitechTest.Web.ViewModels
{
    public class CreateUserViewModel
    {
        [Required]
        [MinLength(ValidationConstans.USER_MAX_LENGTH)]
        [RegularExpression(ValidationConstans.ONLY_ALPHA_NUMERIC, ErrorMessage = "Use only alhanumeric values")]
        [Remote("CheckUserUniqueness", "User", ErrorMessage = "User with this name already exists")]
        public string Name { get; set; }

        [DataType(DataType.Password)]
        [RegularExpression(ValidationConstans.ONE_LOWER_UPER_DIGIT_AT_LEAST_8_CHARS_REST_ALLOWED,
            ErrorMessage = "Password should has mimum 8 characters at least 1 Uppercase Alphabet, 1 Lowercase Alphabet and 1 Number")]
        public string Password { get; set; }
        
        [Display(Name = "Confirm password")]
        [DataType(DataType.Password)]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        [RegularExpression(ValidationConstans.ONE_LOWER_UPER_DIGIT_AT_LEAST_8_CHARS_REST_ALLOWED,
            ErrorMessage = "Password should has mimum 8 characters at least 1 Uppercase Alphabet, 1 Lowercase Alphabet and 1 Number")]
        public string ConfirmPassword { get; set; }

        public bool Success { get; set; }

        public User ToModel()
        {
            if (!string.Equals(Password, ConfirmPassword, StringComparison.InvariantCulture))
            {
                throw new ValidationException("The password and confirmation password do not match. ");
            }
            return new User(Name, Password);
        }
    }
}
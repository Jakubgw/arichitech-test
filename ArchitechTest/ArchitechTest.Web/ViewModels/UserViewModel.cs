﻿namespace ArchitechTest.Web.ViewModels
{
    public class UserViewModel
    {
        public long Id { get;  }
        public string Name { get; }


        public UserViewModel()
        {
        }

        public UserViewModel(long id, string name)
        {
            Id = id;
            Name = name;
        }
    }
}
﻿using System.Data.Entity;
using System.Reflection;
using System.Runtime.Remoting.Messaging;
using System.Web.Mvc;
using ArchitechTest.Data;
using ArchitechTest.Model;
using ArchitechTest.Web.Controllers;
using Autofac;
using Autofac.Integration.Mvc;

namespace ArchitechTest.Web
{
    public class IocConfig
    {
        public static void Configure()
        {
            var builder = new ContainerBuilder();

            
            builder.RegisterControllers(typeof(MvcApplication).Assembly);

            builder.Register((c, p) =>
                 new Repository<User>(p.Named<dynamic>("database")))
                    .As<IRepository<User>>();

            builder.Register(context => new AppDbContext("test")).As<DbContext>();
            builder.RegisterType<RepostioryFactory>().As<IRepositoryFactory>();
            builder.RegisterType<UnitOfWork>().As<IUnitOfWork>();
     

            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        } 
    }
}
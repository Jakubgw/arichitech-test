﻿using System.Data.Entity;
using ArchitechTest.Model;

namespace ArchitechTest.Data
{
    public interface IRepositoryFactory
    {
        IRepository<TEntity> GetRepositoryForEntityType<TEntity>(DbContext dbContext) where TEntity : class;

    }
}
﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using ArchitechTest.Model;

namespace ArchitechTest.Data
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(string nameOrConnectionString) : base(nameOrConnectionString)
        {

            Database.SetInitializer<AppDbContext>(null);
        }

        public DbSet<User> Users { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}
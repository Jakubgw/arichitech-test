﻿using System.Data.Entity;
using ArchitechTest.Model;

namespace ArchitechTest.Data
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly DbContext dbContext;
        public UnitOfWork(IRepositoryFactory repositoryFactory, DbContext dbContext)
        {
            this.dbContext = dbContext;
            this.Users = repositoryFactory.GetRepositoryForEntityType<User>(dbContext);
        }

        public IRepository<User> Users { get; }

        public void Commit()
        {
            dbContext.SaveChanges();
        }

    }
}
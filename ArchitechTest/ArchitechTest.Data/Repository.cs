﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using ArchitechTest.Model;


namespace ArchitechTest.Data
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity :class
    {
        protected DbContext DbContext { get; set; }

        protected DbSet<TEntity> DbSet { get; set; }

        public Repository(DbContext dbContext)
        {
            DbContext = dbContext;
            DbSet = dbContext.Set<TEntity>();
        }

        public IQueryable<TEntity> GetAll()
        {
            return DbSet;
        }

        public TEntity GetById(int id)
        {
            throw new System.NotImplementedException();
        }

        public void Add(TEntity entity)
        {
            ValidateEntity(entity);
            DbEntityEntry dbEntityEntry = DbContext.Entry(entity);
            if (dbEntityEntry.State != EntityState.Detached)
            {
                dbEntityEntry.State = EntityState.Added;
            }
            else
            {
                DbSet.Add(entity);
            }
        }

        public void Update(TEntity entity)
        {
            throw new System.NotImplementedException();
        }

        public void Delete(TEntity entity)
        {
            throw new System.NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new System.NotImplementedException();
        }

        private void ValidateEntity(TEntity entity)
        {
            var errors = new List<ValidationResult>();
            Validator.TryValidateObject(entity, new ValidationContext(entity), errors, true);

            if (errors.Any())
            {
                throw new ValidationException(errors.First(), null, this);
            }
        }
    }
}
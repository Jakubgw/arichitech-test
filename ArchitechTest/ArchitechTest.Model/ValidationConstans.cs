﻿namespace ArchitechTest.Model
{
    public static class ValidationConstans
    {
        public const string ONE_LOWER_UPER_DIGIT_AT_LEAST_8_CHARS_REST_ALLOWED = @"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d._^%$#!~@,-]{8,}$";
        public const string ONLY_ALPHA_NUMERIC = @"^[0-9a-zA-Z ]+$";

        public const int USER_MAX_LENGTH = 5;
    }
}
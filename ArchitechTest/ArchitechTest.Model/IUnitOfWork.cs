﻿using System;

namespace ArchitechTest.Model
{
    public interface IUnitOfWork
    {
        IRepository<User> Users { get; } 

        void Commit();
    }
}
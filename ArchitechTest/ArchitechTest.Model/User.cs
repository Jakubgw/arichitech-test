﻿using System.ComponentModel.DataAnnotations;

namespace ArchitechTest.Model
{
    public class User
    {
        public long Id { get; set; }

        [Required]
        [MinLength(ValidationConstans.USER_MAX_LENGTH)]
        [RegularExpression(ValidationConstans.ONLY_ALPHA_NUMERIC)]
        public string Name { get; set; }
            
        [RegularExpression(ValidationConstans.ONE_LOWER_UPER_DIGIT_AT_LEAST_8_CHARS_REST_ALLOWED)]
        public string Password { get; set; }

        public User()
        {
        }

        public User(string name, string password)
        {
            Name = name;
            Password = password;
        }
    }
}
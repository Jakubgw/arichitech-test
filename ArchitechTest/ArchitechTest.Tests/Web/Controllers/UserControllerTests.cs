﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using ArchitechTest.Model;
using ArchitechTest.Web.Controllers;
using ArchitechTest.Web.ViewModels;
using Moq;
using NUnit.Framework;

namespace ArchitechTest.Tests.Web.Controllers
{
    [TestFixture]
    public class UserControllerTests
    {
        private Mock<IUnitOfWork> unitOfWork;
        private Mock<IRepository<User>> userRepository;

        [SetUp]
        public void Init()
        {
            unitOfWork = new Mock<IUnitOfWork>();
            userRepository = new Mock<IRepository<User>>();
            unitOfWork.Setup(item => item.Users).Returns(userRepository.Object);
        }

        [Test]
        public void Shouldnt_Create_User_If_Model_Is_Invalid ()
        {
            userRepository.Setup(item => item.Add(It.IsAny<User>())).Throws(new Exception("State is invalid !"));

            var userController = new UserController(unitOfWork.Object);

            var createUserViewModel = new CreateUserViewModel();

            userController.ViewData.ModelState.AddModelError("Key", "ErrorMessage");

            userController.Create(createUserViewModel);
        }

        [Test]
        public void Should_Add_And_Commit_User()
        {
            unitOfWork.Setup(item => item.Commit()).Verifiable("Commit was not called!");
            userRepository.Setup(item => item.Add(
                It.Is<User>(user => user.Name == "test" && user.Password == "testpassword")
                )).Verifiable("User object was not added!");

            var userController = new UserController(unitOfWork.Object);

            var createUserViewModel = new CreateUserViewModel();
            createUserViewModel.Name = "test";
            createUserViewModel.Password = "testpassword";
            createUserViewModel.ConfirmPassword = "testpassword";

            userController.Create(createUserViewModel);
            userRepository.VerifyAll();
            unitOfWork.VerifyAll();
        }

        [Test]
        public void Should_Return_Correct_ViewModel_After_Create()
        {
            var userController = new UserController(unitOfWork.Object);

            var createUserViewModel = new CreateUserViewModel();
            createUserViewModel.Name = "test";
            createUserViewModel.Password = "testpassword";
            createUserViewModel.ConfirmPassword = "testpassword";

            var result = userController.Create(createUserViewModel);

            var model = ((ViewResult)result).Model as CreateUserViewModel;

            Assert.AreEqual("test", model.Name);
            Assert.AreEqual("testpassword", model.Password);
            Assert.AreEqual(true, model.Success);

        }

        [Test]
        [ExpectedException(typeof(ValidationException))]
        public void Should_Throws_ValidationException_If_Passwords_Not_Match()
        {
            var userController = new UserController(unitOfWork.Object);

            var createUserViewModel = new CreateUserViewModel();
            createUserViewModel.Name = "test";
            createUserViewModel.Password = "testpassword";

            userController.Create(createUserViewModel);
        }

        [Test]
        public void Should_Check_User_Uniqueness()
        {
            var users = new List<User>()
            {
                new User("user1","pass")
            }.AsQueryable();
            userRepository.Setup(item => item.GetAll()).Returns(users);

            var userController = new UserController(unitOfWork.Object);

            var userExists = userController.CheckUserUniqueness("user1") as JsonResult;
            var userDoesNotExist = userController.CheckUserUniqueness("user2") as JsonResult;

            Assert.AreEqual(userExists.Data, false);
            Assert.AreEqual(userDoesNotExist.Data, true);
        }

        [Test]
        public void Should_Return_All_Users()
        {

            var users = new List<User>()
            {
                new User("user1","pass"),
                new User("user2","pass")
            }.AsQueryable();

            userRepository.Setup(item => item.GetAll()).Returns(users);


            var userController = new UserController(unitOfWork.Object);

            var result = userController.Index();
            var model = ((ViewResult)result).Model as IEnumerable<UserViewModel>;

            Assert.AreEqual(2,model.Count());
            Assert.AreEqual("user1", model.First().Name);
            Assert.AreEqual("user2", model.Last().Name);
        }

    }
}